//
//  MusicTableViewController+FetchDelegate.m
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//

#import "MusicTableViewController_Private.h"

@implementation MusicTableViewController (FetchDelegate)

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableViewRowAnimation animation = UITableViewRowAnimationAutomatic;
    UITableView *table = self.tableView;
    switch(type)
    {
        case NSFetchedResultsChangeInsert:
            [table insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:animation];
            break;
            
        case NSFetchedResultsChangeDelete:
            [table deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:animation];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [table reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:animation];
            break;
            
        case NSFetchedResultsChangeMove:
            [table deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:animation];
            [table insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:animation];
            break;
    }
}

@end
