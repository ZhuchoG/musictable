//
//  MusicTableViewController+FetchDelegate.h
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//

#import "MusicTableViewController.h"

#import <MagicalRecord/MagicalRecord.h>

@interface MusicTableViewController()

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

@interface MusicTableViewController (FetchDelegate) <NSFetchedResultsControllerDelegate>

@end
