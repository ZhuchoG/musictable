//
//  MusicTableViewController.m
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//

#import "MusicTableViewController.h"
#import "MusicTableViewController_Private.h"

#import "SongsGetAPI.h"
#import "SongModel.h"

@implementation MusicTableViewController

- (IBAction)refreshAction:(id)sender
{
    [self loadSongs];
}

- (void)loadSongs
{
    //https://github.com/alkozin/iOS-API-Wrapper using our API wrapper
    [SongsGetAPI withCompletion:^(id reply, NSError *error, BOOL *handleError) {
        [self.refreshControl endRefreshing];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupDataFetch];
    [self loadSongs];
}

- (void)setupDataFetch
{
    self.fetchedResultsController = [SongModel MR_fetchAllSortedBy:@"label" ascending:YES
                                                     withPredicate:nil groupBy:nil delegate:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.fetchedResultsController.sections[section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellSong forIndexPath:indexPath];
    SongModel *song = [self songAtIndexPath:indexPath];
    
    cell.textLabel.text = song.label;
    cell.detailTextLabel.text = song.author;
    
    return cell;
}

- (SongModel *)songAtIndexPath:(NSIndexPath *)indexPath
{
    SongModel *song = (SongModel *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    return song;
}

@end
