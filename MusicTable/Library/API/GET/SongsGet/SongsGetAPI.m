//
//  SongsGetAPI.m
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//

#import "SongsGetAPI.h"
#import "API_Protected.h"
#import "SongModel.h"

#import <MagicalRecord/MagicalRecord.h>

@implementation SongsGetAPI

- (NSString *)path
{
    return kAPISongsPath;
}

- (void)apiDidReturnReply:(id)reply source:(id)source
{
    if ([reply isKindOfClass:[NSArray class]]) {
        NSArray *songsArray = (NSArray *)reply;
        
        //Select only reply objects what not in DB
        NSArray *currentSongsUids = [[SongModel MR_findAll] valueForKeyPath:@"uid"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"not (%K IN %@)",@"id", currentSongsUids];
        songsArray = [songsArray filteredArrayUsingPredicate:predicate];
        
        [SongModel MR_importFromArray:songsArray];
        [self removeDeletedForReply:reply];
        
        //Dont need to save context at this point, save it on app background or termination
    }
    
    [super apiDidReturnReply:reply source:source];
}

- (void)removeDeletedForReply:(id)reply
{
    //Select DB objects what not in reply
    NSArray *uids = [(NSArray *)reply valueForKeyPath:@"id"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"not (%K IN %@)",@"uid", uids];
    [SongModel MR_deleteAllMatchingPredicate:predicate];
}

@end
