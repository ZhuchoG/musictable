//
//  API+RequestProgress.m
//  Sample
//
//  Created by Alexander Kozin on 04.07.15.
//  Copyright © 2015 Siberian.pro. All rights reserved.
//

#import "API_Protected.h"

@implementation API (RequestProgress)

- (void)sendRequestWithCompletion:(void (^)(id reply, NSError *error, BOOL *handleError))completion
{
    [self setCompletion:completion];

    if ([API networkIsReachable]) {
        [self sendRequest];
    } else {
        [self invokeApiNotReachableSequence];
    }
}

- (void)sendRequest
{
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(globalQueue, ^{
        AFHTTPSessionManager *client = [API httpClient];

        NSMutableURLRequest *request = [self createRequest];
        
        NSURLSessionDataTask *task;
        task = [client dataTaskWithRequest:request
                         completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                             self.responseStatusCode = ((NSHTTPURLResponse *)response).statusCode;
                             if (self.shouldLogRequest) {
                                 NSLog(@"%@",responseObject);
                             }
                                if (error) {
                                     NSError *apiError = [self apiErrorFromNetworkError:error];
                                    
                                    id errors;  // is array only
                                    id err = responseObject[@"error"];
                                    if ([err isKindOfClass:[NSDictionary class]]){
                                        errors = [err allValues];
                                    }
                                    else if ([err isKindOfClass:[NSString class]]){
                                        errors = @[err];
                                    }
                                    else {  // if array
                                        errors = err;
                                    }

                                     if (errors) {
                                         NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:apiError.userInfo];
                                         userInfo[@"errors"] = errors;
                                         apiError = [NSError errorWithDomain:apiError.domain code:apiError.code userInfo:userInfo];
                                     }
                                    [self apiDidFailWithError:apiError];
                                 
                             
                                } else {
                                 [self requestDidReturnReply:responseObject];
                                }
                         }];
        
        [task resume];
        [self setSessionTask:task];

        if (self.shouldLogRequest) {
            NSString *requestString = [NSString stringWithFormat:@"#AF request %@: %@\nHeaders: %@\n Parameters: %@", self.method,
                                       request.URL,
                                       request.allHTTPHeaderFields,
                                       [self parameters]];
            if (requestString.length > 10000) {
                requestString = [NSString stringWithFormat:@"%@...", [requestString substringToIndex:10000]];
            }
            NSLog(@"%@",requestString);
        }
    });
}

- (void)requestDidReturnReply:(id)reply
{
    id parsedReply = reply;

    Class classForParcingReply = [self classForParsingReply];
    if (classForParcingReply) {
        parsedReply = [classForParcingReply objectFromReply:reply];
    }

    [self apiDidReturnReply:parsedReply source:reply];
}

- (Class)classForParsingReply
{
    return NULL;
}

- (void)cancel
{
    [self.sessionTask cancel];
}

- (BOOL)apiRequestInProgress
{
    return self.sessionTask != nil;
}

@end
