//
//  Constants.m
//

#import "Constants.h"

NSString
// API URLs
*const kAPIBaseURL = @"http://tomcat.kilograpp.com/songs/api/",
*const kAPISongsPath = @"songs",

//Table cells
*const kCellSong = @"songCell"
;
