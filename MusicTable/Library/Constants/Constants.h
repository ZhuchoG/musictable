//
//  Constants.h
//

FOUNDATION_EXPORT NSString

// API URLs
*const kAPIBaseURL,
*const kAPISongsPath,

//Table cells
*const kCellSong
;
