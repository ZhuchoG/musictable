//
//  AppDelegate.h
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

