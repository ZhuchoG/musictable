//
//  BaseModel.h
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseModel : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BaseModel+CoreDataProperties.h"
