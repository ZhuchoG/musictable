//
//  SongModel.h
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SongModel : BaseModel

@end

NS_ASSUME_NONNULL_END

#import "SongModel+CoreDataProperties.h"
