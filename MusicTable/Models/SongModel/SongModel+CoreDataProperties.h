//
//  SongModel+CoreDataProperties.h
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SongModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SongModel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *author;
@property (nullable, nonatomic, retain) NSString *label;
@property (nullable, nonatomic, retain) NSNumber *version;

@end

NS_ASSUME_NONNULL_END
