//
//  SongModel+CoreDataProperties.m
//  MusicTable
//
//  Created by Vitaliy Zhukov on 10.06.16.
//  Copyright © 2016 Vitaliy Zhukov. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SongModel+CoreDataProperties.h"

@implementation SongModel (CoreDataProperties)

@dynamic author;
@dynamic label;
@dynamic version;

@end
