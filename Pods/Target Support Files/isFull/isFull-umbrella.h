#import <UIKit/UIKit.h>

#import "isFull.h"
#import "NSObject+isFull.h"

FOUNDATION_EXPORT double isFullVersionNumber;
FOUNDATION_EXPORT const unsigned char isFullVersionString[];

